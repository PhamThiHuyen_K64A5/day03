<?php
echo "
<!DOCTYPE html>
<html lang='en'>

<head>
    <meta charset='UTF-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <title>Document</title>

    <style>
        #body {
            width: 390px;
            height: 255px;
            border: solid #547FA5 2px;
            padding: 64px 40px 10px 40px;
        }

        .gender {
            margin: 0px 10px 0px 15px;
        }

        .frames {
            color: black;
            border: solid #41719C 2px;
            height: 30px;
            width: 80%;
        }

        table {
            border-collapse: separate;
            border-spacing: 15px 15px;
        }

        label {
            font-size: 14px;
            font-family: 'Times New Roman', Times, serif;
        }

        .background_blue {
            color: aliceblue;
            background-color: #5B9BD5;
            padding: 10px 10px 0px 10px;
            border: solid #41719C 2px;
        }

        .blue-box {
            border: #5B9BD5 solid 2px;
            line-height: 20px;
            height: 30px;
            width: 80%;
        }
        button {
            background-color: #70AD47;
            color: aliceblue;
            justify-content: center;
            padding: 10px 20px 10px 20px;
            width: 30%;
            border-radius: 7px;
            font-family: 'Times New Roman', Times, serif;
            font-size: 14px;
            border: solid #41719C 2px;
            margin: 5% 0% 0% 40%;
        }
    </style>
</head>s

<body>
    <div id='body'>
        <table>
            <tr>
                <td class='background_blue'>
                    Họ và tên
                </td>
                <td>
                    <input type='text' class='frames' style=' width: 150%;'>
                </td>
            </tr>

            <tr>
                <td class='background_blue'>
                    Giới tính
                </td>
                <td>";
                $gender = array(0 => 'Nam', 1 => 'Nữ');
                for ($i = 0; $i < 2; $i++) {
                    echo"<input type='radio' id='".$i."' name='gender' value='".$gender[$i]."'/> ".$gender[$i];
                }
                echo"</td>
            </tr>

            <tr>
                <td class='background_blue'>
                    Phân khoa
                </td>
                <td> <select class='blue-box' id='falcuty'>";
                $falcuty = array('' => '', 'MAT' => 'Khoa học máy tính', 'KDL' => 'Khoa học vật liệu');
                foreach($falcuty as $key => $value) {
                    echo " <option value =".$key.">".$value."</option> ";
                }
                echo "</td>
                </td>
            </tr>
        </table>
        <button>
            Đăng ký
        </button>
    </div>

</body>

</html>";
